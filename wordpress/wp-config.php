<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //


/**
*  The name of the database for WordPress
*/
define( 'DB_NAME', 'wordpress' );

/**
*  MySQL database username
*/
define( 'DB_USER', 'root' );

/**
*  MySQL database username
*/
define( 'DB_PASSWORD', 'password' );

/**
*  MySQL hostname
*/
define( 'DB_HOST', 'mysql' );

/**
*  Database Charset to use in creating database tables.
*/
define('DB_CHARSET', 'utf8');

/**
*  The Database Collate type. Don't change this if in doubt.
*/
define('DB_COLLATE', '');

/**
*  WordPress Database Table prefix.
*  You can have multiple installations in one database if you give each a unique
*  prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'wp_';

/**
*  disallow unfiltered HTML for everyone, including administrators and super administrators. To disallow unfiltered HTML for all users, you can add this to wp-config.php:
*/
define('DISALLOW_UNFILTERED_HTML', false);

/**
*  
*/
define('ALLOW_UNFILTERED_UPLOADS', false);

/**
*  The easiest way to manipulate core updates is with the WP_AUTO_UPDATE_CORE constant
*/
define('WP_AUTO_UPDATE_CORE', true);

/**
*  Authentication Unique Keys and Salts.
*  Change these to different unique phrases!
*  You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
*  You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*  @since 2.6.0
*/
define('AUTH_KEY', 'c76275ebb251dccede58aef209f55882818b542f');
define('SECURE_AUTH_KEY', 'dadb25d2dfc24482a742fc9213acdbe2bde90bbb');
define('LOGGED_IN_KEY', '23fa79c4b1a7a18f753f891a19870243e69760ea');
define('NONCE_KEY', '2f78aaa6149f8d54a5728f40972bad39c5512fbc');
define('AUTH_SALT', 'fc8d689e749f3afe229823cb688f972698896bfb');
define('SECURE_AUTH_SALT', '12893d88736c5318761a87bbd0f036b0ebb15f40');
define('LOGGED_IN_SALT', '2d9d2b8ef95bcf8534d1327d154c3eedfabc21e2');
define('NONCE_SALT', 'd4a5084ac5e00ad961f5984cf8f8bc74633ae05f');

/**
*  For developers: WordPress debugging mode.
*  Change this to true to enable the display of notices during development.
*  It is strongly recommended that plugin and theme developers use WP_DEBUG
*  in their development environments.
*/
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', true);

/**
*  For developers: WordPress Script Debugging
*  Force Wordpress to use unminified JavaScript files
*/
define('SCRIPT_DEBUG', false);

/**
*  WordPress Localized Language, defaults to English.
*  Change this to localize WordPress. A corresponding MO file for the chosen
*  language must be installed to wp-content/languages. For example, install
*  de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
*  language support.
*/
define('WPLANG', '');

/**
*  Setup Multi site
*/
define('WP_ALLOW_MULTISITE', false);

define('FS_METHOD', 'direct' );

/**
*  Post Autosave Interval
*/
define('AUTOSAVE_INTERVAL', 60);

/**
*  Disable / Enable Post Revisions and specify revisions max count
*/
define('WP_POST_REVISIONS', true);

/**
*  this constant controls the number of days before WordPress permanently deletes 
*  posts, pages, attachments, and comments, from the trash bin
*/
define('EMPTY_TRASH_DAYS', 30);

/**
*  Disable the cron entirely by setting DISABLE_WP_CRON to true.
*/
define('DISABLE_WP_CRON', false);

/**
*  Make sure a cron process cannot run more than once every WP_CRON_LOCK_TIMEOUT seconds
*/
define('WP_CRON_LOCK_TIMEOUT', 120);
define('ALTERNATE_WP_CRON', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
